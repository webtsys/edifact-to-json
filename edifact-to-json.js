
module.exports = function(RED) {
    function EdifactToJson(config) {
        
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
            /*msg.payload = msg.payload.toLowerCase();
            node.send(msg);*/
            
            filename=msg.payload;
            
            read_edifact(filename, 'order', function(order, order_lines) {
                
                msg.payload=[order, order_lines];
                
                node.send(msg);
                
            });
            
        });
    }
    RED.nodes.registerType("edifact-to-json",EdifactToJson);
}

/**
* Node red script for 
* 
*
**/

function read_edifact(filename, type_edi, callback) {

    var Reader = require('edifact/reader.js');

    var fs = require('fs');

    let reader = new Reader({ autoDetectEncoding: true });

    file=fs.readFile(filename, 'utf8', (err, data) => {
      
        if (err) throw err;
        
        if(type_edi=='order') {
            
            //console.log(data);
            
            arr_data=data.split('\n')
            
            //console.log(arr_data)
            
            final_data='';
            
            for(var z in arr_data) {
            
                if(arr_data[z].indexOf('@')=='-1') {
                    
                    final_data+=arr_data[z].trim()+"\n"
                
                }
            
            }
            
            data=final_data;
            
            var order={};
            
            var order_lines={};
            
            //CLAVE_GENERADA|TIPO_PEDIDO|FUNCION_PEDIDO|NUM_PEDIDO_EMISOR|FECHA_EMISION_PEDIDO|PRIMERA_FECHA_ENTREGA_PEDIDO|ULTIMA_FECHA_ENTREGA_PEDIDO|FECHA_TOPE_PEDIDO|||||||EMISOR|COMPRADOR|CLIENTE_FACTURA||RECEPTOR||VENDEDOR|QPAGA|||||EUR||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
            /*//cabped[z]=cabped[z].replace('TIPO_PEDIDO', 220);
    
            //La función del pedido, por defecto es 9 que significa que 
            //es pedido original, otros códigos son 3, que sería cancelación
            // de pedido, pero por defecto por ahora usaremos 9
    
            cabped[z]=cabped[z].replace('FUNCION_PEDIDO', 9);*/
      
            let result = reader.parse(data);
            
            let last_line='';
            
            for(var i in result) {
                
                // Get num order In bgm
                /**
                * BGM BEGINNING OF MESSAGE To indicate the type and function of a message and to transmit the identifying number.
                *
                *    220: Order Document/message by means of which a buyer initiates a transaction with a seller involving the supply of goods or services as specified, according to conditions set out in an offer, or otherwise *known to the buyer. Esto indica que es un pedido.
                *
                * 54664701: DOCUMENT/MESSAGE NUMBER Numero de pedido?
                *
                *9: Original Initial transmission related to a given transaction. Pedido original. 
                **/
                
                if(result[i]['name']=='BGM') {
                    
                    //console.log(result[i]);
                    order['TIPO_PEDIDO']=result[i]['elements'][0][0];
                    
                    if(result[i]['elements'].hasOwnProperty(2)) {
                    
                        order['FUNCION_PEDIDO']=result[i]['elements'][2][0];
                        
                    }
                    else {
                        
                        order['FUNCION_PEDIDO']=9;
                        
                    }
                    
                    order['NUM_PEDIDO_EMISOR']=result[i]['elements'][1][0];
                    
                }
                
                /*
                 * { name: 'DTM', elements: [ [ '137', '20201229', '102' ] ] }
                    { name: 'DTM', elements: [ [ '2', '20210108', '102' ] ] }
                    { name: 'DTM', elements: [ [ '63', '20210108', '102' ] ] }
                    
                    DTM+137:20201229:102' Date/Time/Period ¿Fecha pedido? 137 Document/message date/time (2006) Date/time when a document/message is issued. This may include authentication Cuando se abre el pedido.
                    DTM+2:20210108:102' Delivery date/time, requested Date on which buyer requests goods to be delivered. Cuando el comprador quiere la mercancia.
                    DTM+63:20210108:102' 63	Delivery date/time, latest Date identifying a point of time after which goods shall not or will not be delivered. La ultima fecha de pedido convenida, como en edicon.

                */
                
                //Habrá que ponerle un filtro para el tipo de fecha. 
                
                if(result[i]['name']=='DTM') {
                    
                    //console.log(result[i]);
                    
                    console.log(result[i]['elements'][0][0]);
                    
                    if(result[i]['elements'][0][0]=='137') {
                        
                        order['FECHA_EMISION_PEDIDO']=result[i]['elements'][0][1];
                        
                    }
                    
                    if(result[i]['elements'][0][0]=='2') {
                        
                        order['PRIMERA_FECHA_ENTREGA_PEDIDO']=result[i]['elements'][0][1];
                        
                    }
                    
                    if(result[i]['elements'][0][0]=='63') {
                        
                        order['ULTIMA_FECHA_ENTREGA_PEDIDO']=result[i]['elements'][0][1];
                        
                    }
                    
                }
                
                /*
                 * NAD+MS+8422416000016::9' MS	Document/message issuer/sender Issuer of a document and/or sender of a message El que hace el pedido.
                    NAD+MR+8425948000004::9' MR Message recipient Self explanatory. El que recibe el pedido.
                    NAD+SU+8425948000004::9' SU	Supplier(3280) Party which manufactures or otherwise has possession of goods, and consigns or makes them available in trade. El que suministra el producto (a.k.a redipro)
                    NAD+DP+8422416200508::9' DP	Delivery party (3144) Party to which goods should be delivered, if not identical with consignee. A quién se libera, esto es, quién recibe la mercancia. 
                    NAD+BY+8422416200508::9' BI	Buyer's financial institution Financial institution designated by buyer to make payment. Entiendo que es el que factura. QPAGA
                    * NAD+IV+8422416000016::9' IV	Invoicee (3006) Party to whom an invoice is issued Aquién se envía la factura. 
                    { name: 'NAD', elements: [ [ 'MS' ], [ '8422416000016', '', '9' ] ] }
                    { name: 'NAD', elements: [ [ 'MR' ], [ '8425948000004', '', '9' ] ] }
                    { name: 'NAD', elements: [ [ 'SU' ], [ '8425948000004', '', '9' ] ] }
                    { name: 'NAD', elements: [ [ 'DP' ], [ '8422416200621', '', '9' ] ] }
                    { name: 'NAD', elements: [ [ 'BY' ], [ '8422416200621', '', '9' ] ] }
                    { name: 'NAD', elements: [ [ 'IV' ], [ '8422416000016', '', '9' ] ] }
                    */
                    
                if(result[i]['name']=='NAD') {
                
                    if(result[i]['elements'][0]=='MS') {
                        
                        order['COMPRADOR']=result[i]['elements'][1][0];
                        
                    }
                    
                    if(result[i]['elements'][0]=='MR') {
                        
                        order['VENDEDOR']=result[i]['elements'][1][0];
                        
                    }
                    
                    if(result[i]['elements'][0]=='SU') {
                        
                        order['SUMINISTRADOR']=result[i]['elements'][1][0];
                        
                    }

                    if(result[i]['elements'][0]=='DP') {
                        
                        order['RECEPTOR']=result[i]['elements'][1][0];
                        
                    }
                    
                    if(result[i]['elements'][0]=='BY') {
                        
                        order['QPAGA']=result[i]['elements'][1][0];
                        order['EMISOR']=result[i]['elements'][1][0];
                        
                    }
                    
                    /*if(result[i]['elements'][0]=='IV') {
                        
                        order['EMISOR']=result[i]['elements'][1][0];
                        
                    }*/
                
                }
                
                //Tax
                //TAX+7+VAT+++:::21.0000'
                //{ name: 'CUX', elements: [ [ '2', 'EUR', '9' ] ] }
                
                if(result[i]['name']=='TAX') {
                    
                    //console.log(result[i]);
                    if(result[i]['elements'][1]=='VAT') {
                        
                        order['VAT']=result[i]['elements'][4][3];
                        
                    }
                    
                }
                
                //Moneda
                //{ name: 'CUX', elements: [ [ '2', 'EUR', '9' ] ] }
                
                if(result[i]['name']=='CUX') {
                    
                    
                    if(result[i]['elements'][0][0]=='2') {
                        
                        order['MONEDA']=result[i]['elements'][0][1];
                        
                    }
                    
                }
                
                /*
                 * QTY+59:1' 
                    59	
                    Numbers of consumer units in the traded unit
                    Self explanatory.

                    QTY+21:280'
                    21	
                    Ordered quantity
                    The quantity which has been ordered.
                    */
                //CLAVE_GENERADA|NUM_LINEA_DE_PEDIDO|EAN13|||ASIN|ASIN2||DESCRIPCION_PRODUCTO|DESCRIPCION_PRODUCTO||CANTIDAD_PRODUCTO|||CANTIDAD_CONSUMO_DISTRIBUCION|||PRECIO_NETO|PRECIO_BRUTO|PVP|NETO|A|01|TD|DESCUENTO_PORC||||||||||||||||||||||||||NUM_UNITS_CONSUMO|||||MARCA_SEGUN_CI|||||||||
                //Lineas
                //  elements: [ [ '1' ], [ '' ], [ '8425948857332', 'EN' ] ]
                
                if(result[i]['name']=='LIN') {
                    
                    last_line=order['NUM_PEDIDO_EMISOR'];
                    
                    if(!order_lines.hasOwnProperty(last_line)) {
                    
                        order_lines[last_line]=[];
                        
                    }
                    
                    order_lines[last_line].push({});
                    
                    num_line=order_lines[last_line].length-1;
                    
                    order_lines[last_line][num_line]['NUM_LINEA_PEDIDO']=result[i]['elements'][0][0];
                    
                    if(result[i]['elements'][2][1]=='EN') {
                        
                        order_lines[last_line][num_line]['EAN13']=result[i]['elements'][2][0];
                        
                    }
                    
                }
                
                //Cantidad de productos
                
                    /*QTY+59:1' 
                    59	
                    Numbers of consumer units in the traded unit
                    Self explanatory.

                    QTY+21:280'
                    21	
                    Ordered quantity
                    The quantity which has been ordered.
                    * 
                    *{ name: 'QTY', elements: [ [ '59', '1' ] ] }
                    { name: 'QTY', elements: [ [ '21', '120' ] ] }

                    * 
                    */
                    
                if(result[i]['name']=='QTY') {
                    
                    if(result[i]['elements'][0][0]=='21') {
                        
                        order_lines[last_line][num_line]['CANTIDAD_PRODUCTO']=result[i]['elements'][0][1];
                        
                    }
                    
                }
                
                /**
                 * 
                 * { name: 'PRI', elements: [ [ 'AAA', '18.66' ] ] }
                    { name: 'PRI', elements: [ [ 'AAB', '18.66' ] ] }
                    { name: 'PRI', elements: [ [ 'INF', '44.95', '', 'CU' ] ] }
                    */                
                //LO que sigue a las lineas
                
                if(result[i]['name']=='PRI') {
                    
                    if(result[i]['elements'][0][0]=='AAA') {
                        
                        order_lines[last_line][num_line]['PRECIO_NETO']=result[i]['elements'][0][1];
                        
                    }
                    
                    if(result[i]['elements'][0][0]=='AAB') {
                        
                        order_lines[last_line][num_line]['PRECIO_BRUTO']=result[i]['elements'][0][1];
                        
                    }
                    
                }
                
                
            }
            
            return callback(order, order_lines);
      
        }
      
    });
    
    

};
